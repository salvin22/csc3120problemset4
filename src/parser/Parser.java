package parser;

import java.util.LinkedList;
import lexer.Lexer;
import lexer.TokenType;
import lexer.Token;
import ast.SyntaxTree;
import ast.nodes.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import ast.nodes.ListNode;


/**
 * Implements a generic super class for parsing files.
 * @author Zach Kissel
 */
public class Parser
{
  private Lexer lex;            // The lexer for the parser.
  private boolean errorFound;   // True if ther was a parser error.
  private boolean doTracing;    // True if we should run parser tracing.
  private Token nextTok;        // The current token being analyzed.

  /**
   * Constructs a new parser for the file {@code source} by
   * setting up lexer.
   * @param src the source code file to parse.
   * @throws FileNotFoundException if the file can not be found.
   */
  public Parser(File src) throws FileNotFoundException
  {
    lex = new Lexer(src);
    errorFound = false;
    doTracing = false;
  }

  /**
   * Construct a parser that parses the string {@code str}.
   * @param str the code to evaluate.
   */
  public Parser(String str)
  {
    lex = new Lexer(str);
    errorFound = false;
    doTracing = false;
  }

  /**
   * Turns tracing on an off.
   */
  public void toggleTracing()
  {
    doTracing = !doTracing;
  }

  /**
   * Determines if the program has any errors that would prevent
   * evaluation.
   * @return true if the program has syntax errors; otherwise, false.
   */
  public boolean hasError()
  {
    return errorFound;
  }

  /**
   * Parses the file according to the grammar.
   * @return the abstract syntax tree representing the parsed program.
   */
  public SyntaxTree parse()
  {
    SyntaxTree ast;

    nextToken();    // Get the first token.
    ast = new SyntaxTree(evalExpr());   // Start processing at the root of the tree.

    if (nextTok.getType() != TokenType.EOF)
      logError("Parse error, unexpected token " + nextTok);
    return ast;
  }


  /************
   * Private Methods.
   *
   * It is important to remember that all of our non-terminal processing methods
   * maintain the invariant that each method leaves the next unprocessed token
   * in {@code nextTok}. This means each method can assume the value of
   * {@code nextTok} has not yet been processed when the method begins.
   ***********/

   /**
    * Method to handle the expression non-terminal
    *
    * <expr> -> let <id> := <expr> in <expr> | <term> {(+ | - ) <term>}
    */
    private SyntaxNode evalExpr()
    {
        trace("Enter <expr>");
        SyntaxNode rterm;
        TokenType op;
        SyntaxNode expr = null;

        // Try to handle a let.
        if (nextTok.getType() == TokenType.LET)
        {
          nextToken();
          return handleLet();
        }
        else  // Handle an addition or subtraction.
        {
          expr = evalTerm();

          while (nextTok.getType() == TokenType.ADD ||
            nextTok.getType() == TokenType.SUB)
          {
            op = nextTok.getType();
            nextToken();
            rterm = evalTerm();
            expr = new BinOpNode(expr, op, rterm);
          }
        }

        trace("Exit <expr>");

        return expr;
    }

    /**
     * This method handles a let expression
     * <id> := <expr> in <expr>
     * @return a let node.
     */
    private SyntaxNode handleLet()
    {
        Token var = null;
        SyntaxNode varExpr;
        SyntaxNode expr;

        trace("Enter <let>");

        // Handle the identifier.
        if (nextTok.getType() == TokenType.ID)
        {
          var = nextTok;
          nextToken();

          // Handle the assignemnt.
          if (nextTok.getType() == TokenType.ASSIGN)
          {
            nextToken();
            varExpr = evalExpr();

            // Handle the in expr.
            if (nextTok.getType() == TokenType.IN)
            {
              nextToken();
              expr = evalExpr();
              return new LetNode(var, varExpr, expr);
            }
            else
            {
              logError("Let expression expected in, saw " + nextTok + ".");
            }
          }
          else
          {
            logError("Let expression missing assignment!");
          }

        }
        else
          logError("Let expression missing variable.");
        trace("Exit <let>");
        return null;
    }

    /**
     * Method to handle the term non-terminal.
     *
     * <term> -> <factor> {( * | /) <factor>}
     */
     private SyntaxNode evalTerm()
     {
       SyntaxNode rfact;
       TokenType op;
       SyntaxNode term = null;

       trace("Enter <term>");       
       if(nextTok.getType() == TokenType.LIST)
       { 
           term = handleList();
           if(nextTok.getType() == TokenType.CONCAT)
           {
               op = nextTok.getType();
               nextToken();
               rfact = handleList();
               term = new BinOpNode(term,op, rfact);
            }
       } 
       else 
       {
            term = evalFactor();
            while (nextTok.getType() == TokenType.MULT
                    || nextTok.getType() == TokenType.DIV) {
                op = nextTok.getType();
                nextToken();
                rfact = evalFactor();
                term = new BinOpNode(term, op, rfact);
            }
            trace("Exit <term>");
            return term;
        }
       return term;
    }

     /**
      * Method to handle the factor non-terminal.
      *
      * <factor> -> <id> | <int> | <real> | ( <expr> )
      */
      private SyntaxNode evalFactor()
      {
        trace("Enter <factor>");
        SyntaxNode fact = null;

        if (nextTok.getType() == TokenType.ID ||
            nextTok.getType() == TokenType.INT ||
            nextTok.getType() == TokenType.REAL)
        {
            fact = new TokenNode(nextTok);
            nextToken();
        }
        else if (nextTok.getType() == TokenType.HEAD)   /* hd token detected*/
        {
            nextToken();
            if(nextTok.getType() == TokenType.LIST) /*Grab the list the hd will use */
            {
                fact = handleList();/* Grab the list, store in fact */
                if(fact instanceof ListNode)
                {
                    if(!((ListNode) fact).getList().isEmpty()){ /*Verify the list is not empty */
                    HeadNode hdNode = new HeadNode(fact);
                    fact = hdNode;
                    }
                }
            }
            else
                fact = new HeadNode(evalFactor());
        }
        else if (nextTok.getType() == TokenType.TAIL)   /* tl token */
        {
            nextToken();
            if(nextTok.getType() == TokenType.LIST) 
            {
                fact = handleList();    /* Grab the list, store in fact */
                if(fact instanceof ListNode)
                {
                    TailNode tlNode = new TailNode(fact);
                    fact = tlNode;
                }
            }
            else
                fact = new TailNode(evalFactor());
        }
        else if (nextTok.getType() == TokenType.LIST) { // list token found, handle it
            fact = handleList();
        }
        else if (nextTok.getType() == TokenType.LPAREN)
        {
          nextToken();
          fact = evalExpr();

          if (nextTok.getType() == TokenType.RPAREN)
            nextToken();
          else
            logError("Expected \")\" received " + nextTok +".");
        }
        else
        {
          logError("Unexpected token " + nextTok);

          // Recover from poorly formed expression.
          // if (nextTok.getType() == TokenType.RPAREN)
          //   nextToken();
        }

        trace("Exit <factor>");
        return fact;
      }
        

  /**
   * Logs an error to the console.
   * @param msg the error message to dispaly.
   */
   private void logError(String msg)
   {
     System.err.println("Error (" + lex.getLineNumber() + "): " + msg);
     errorFound = true;
   }

   /**
    * This prints a message to the screen on if {@code doTracing} is
    * true.
    * @param msg the message to display to the screen.
    */
    private void trace(String msg)
    {
      if (doTracing)
        System.out.println(msg);
    }

    /**
     * Gets the next token from the lexer potentially logging that
     * token to the screen.
     */
    private void nextToken()
    {
      nextTok = lex.nextToken();

      if (doTracing)
        System.out.println("nextToken: " + nextTok);

    }
    
    /** handeList handles the list case. When the list token is found, the nextToken is incremented, 
    * and this method is invoked. It will process the elements of the list until a Right Paren is located,
    * or an error occurs.
    * @return fact is either a ListNode or a TokenNode
    */
    private SyntaxNode handleList()
    {
        SyntaxNode fact = null;
        
        if(nextTok.getType() == TokenType.LIST)
            nextToken();
        // check for left paren at the start of list
        if (nextTok.getType() == TokenType.LPAREN) 
        { 
            nextToken();
            if (nextTok.getType() == TokenType.INT ||nextTok.getType() == TokenType.REAL)   // read integer or real
            {
                LinkedList<TokenNode> lst = new LinkedList<TokenNode>();
                // Adds it to the linked list.
                lst.add(new TokenNode(nextTok));    
                nextToken();

                // If there is a comma, read the token following it.
                while (nextTok.getType() == TokenType.COMMA) 
                {   
                    // Adds the token to the list if valid.
                    nextToken();
                    if (nextTok.getType() == TokenType.INT ||nextTok.getType() == TokenType.REAL)//
                    {
                        lst.add(new TokenNode(nextTok));
                    }
                    
                    nextToken();
                }

                //Empty List Case
                if (nextTok.getType() == TokenType.RPAREN) 
                {   
                    fact = new ListNode(lst); //Return the linked list as a ListNode
                    nextToken();
                }

                else
                {
                    logError("List expected RPAREN, saw " + nextTok + ".");
                }

                return fact;
            } 

            //Empty List Case
            else if (nextTok.getType() == TokenType.RPAREN) 
            {
                fact = new ListNode(new LinkedList<TokenNode>());
                nextToken();
            }

            else
            {
                logError("Unexpected token " + nextTok); 
            }
        }
        
        else if (nextTok.getType() == TokenType.ID)
        {
            fact = new TokenNode(nextTok);
            nextToken();
        }
            
        //log head/tail called on a non list/ other error
        return fact;
   }
}
