package ast.nodes;

import lexer.Token;
import lexer.TokenType;
import environment.Environment;
import java.util.LinkedList;

/**
 * This node represents a head node. A head node has some list associated with
 * it, but evaluates by returning the first element of the node.
 * @author Nick & Gary
 */
 public class HeadNode extends SyntaxNode
 {
   private SyntaxNode list;
    public HeadNode(SyntaxNode list)
    {
      this.list = list;
    }
    /**
     * Evaluate the node.
     * @param env the environment we should evaluate the
     * node under.
     * @return the object representing the result of the evaluation.
     */
     public Object evaluate(Environment env)
     {       
       if(list instanceof ListNode) /* The object is a ListNode */
       {
            LinkedList theList = ((ListNode)list).getList();
            
            if(theList.isEmpty())   //Emtpy list case
            {
                System.out.println("empty list!");
                return null;
            }
            else    //List is not empty, return the first element.
            {
                return  ((TokenNode) theList.element()).evaluate(env);
            }
       }
       else if(list instanceof TailNode)
       {
            LinkedList theList = new LinkedList();
            Object result = ((TailNode)list).getList();
            if(result instanceof ListNode)
            {
                theList = ((ListNode) result).getList();
                if(theList.isEmpty())   //Emtpy list case
                {
                    return null;
                }
                else    //List is not empty, return the first element.
                {
                    TokenNode temp;
                    temp = ((TokenNode) theList.element());
                    return  ((TokenNode) theList.element()).evaluate(env);
                }
             } else if (result instanceof TokenNode) {
                 LinkedList theListt = (LinkedList) env.lookup(((TokenNode) result).getToken()); 
                 if (theListt.isEmpty() || theList.size() == 1) /* Tail operation fails when size == 0 or 1. */ {
                     System.out.println("Can't find tail of list.");
                     return null;
                 } else /* Otherwise, return a new list that contains the head of the tail */ {
                     LinkedList tailList = new LinkedList();
                     for (int i = 1; i < theListt.size(); i++) {
                         tailList.add(theListt.get(i));
                     }
                     return new ListNode(tailList).getList().get(0);
                 }
             }
             else if (result instanceof LinkedList) {
                 theList = (LinkedList)result;
                 if (theList.isEmpty()) //Emtpy list case
                 {
                     System.out.println("empty list!");
                     return null;
                 } else //List is not empty, return the first element.
                 {
                     LinkedList tailList = new LinkedList();
                     for (int i = 1; i < theList.size(); i++) {
                         tailList.add(theList.get(i));
                     }
                     ListNode tempList = new ListNode(tailList);
                     return ((TokenNode)tempList.getList().get(0)).evaluate(env);
                 }
             }
         }
       
       else if (list instanceof TokenNode) /*The object is a TokenNode*/
       {
           LinkedList theList = (LinkedList) ((TokenNode)list).evaluate(env);
           if(theList == null || theList.isEmpty())
               return null;
           else
           {
               if(theList.element() instanceof Integer)
               {
                   Token temp = new Token(TokenType.INT, theList.element().toString());
                   return new TokenNode(temp).evaluate(env);
               }
               return ((TokenNode) theList.element()).evaluate(env);
           }
       }
       
       return null;
     }
 }
