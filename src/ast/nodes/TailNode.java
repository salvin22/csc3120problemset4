package ast.nodes;

import lexer.Token;
import lexer.TokenType;
import environment.Environment;
import java.util.LinkedList;

/**
 * This node represents a tail node. A tail node has some list associated with
 * it, but evaluates by returning every element except the first element in the list.
 * @author Nick & Gary
 */
 public class TailNode extends SyntaxNode
 {
   private SyntaxNode list;
    public TailNode(SyntaxNode list)
    {
      this.list = list;
    }
    
    public Object getList()
    {
        if(list instanceof ListNode) /*List is a ListNode */
        {
           LinkedList theList = ((ListNode)list).getList();
           return theList;
        }
        else
        {
            return list;
        }
    }

    /**
     * Evaluate the node.
     * @param env the executional environment we should evaluate the
     * node under.
     * @return the object representing the result of the evaluation.
     */
     public Object evaluate(Environment env)
     {        
         LinkedList theList = null;
         boolean valid = true;
         
       if(list instanceof ListNode) /*List is a ListNode */
       {
            theList = ((ListNode)list).getList();
       }
       else if (list instanceof TokenNode)
       {
            theList = (LinkedList)((TokenNode)list).evaluate(env);
       }
       else
       {
           valid = false;
       }
       
            if(valid == false)
                return null;
            
            if(theList.isEmpty() || theList.size() == 1)    /* Tail operation fails when size == 0 or 1. */
            {
                System.out.println("Can't find tail of list.");
                return null;
            }
            else    /* Otherwise, return a new list that contains the tail */
            {
                LinkedList tailList = new LinkedList();
                for(int i = 1; i < theList.size(); i++)
                {
                    tailList.add(theList.get(i));
                }
                return new ListNode(tailList).evaluate(env);
            }
     }
 }
