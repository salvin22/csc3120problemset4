package ast.nodes;

import lexer.Token;
import lexer.TokenType;
import environment.Environment;
import java.util.LinkedList;

/**
 * This node represents the program nonterminal.
 * @author Zach Kissel
 */
 public class BinOpNode extends SyntaxNode
 {
   private TokenType op;
   private SyntaxNode leftTerm;
   private SyntaxNode rightTerm;

   /**
    * Constructs a new binary operation syntax node.
    * @param lterm the left operand.
    * @param op the binary operation to perform.
    * @param rterm the right operand.
    */
    public BinOpNode(SyntaxNode lterm, TokenType op, SyntaxNode rterm)
    {
      this.op = op;
      this.leftTerm = lterm;
      this.rightTerm = rterm;
    }

    /**
     * Evaluate the node.
     * @param env the executional environment we should evaluate the
     * node under.
     * @return the object representing the result of the evaluation.
     */
     public Object evaluate(Environment env)
     {
        Object lval;
        Object rval;
        boolean useDouble = false;

        lval = leftTerm.evaluate(env);
        rval = rightTerm.evaluate(env);

        // If either value is null, we can't proceed.
        if (lval == null || rval == null)
          return null;

        // Make sure the type is sound.
        if(!(lval instanceof Integer || lval instanceof Double || lval instanceof LinkedList) &&
           !(rval instanceof Double  || rval instanceof Integer || rval instanceof LinkedList))
          return null;
        if (lval.getClass() !=  rval.getClass())
        {
          System.out.println("Error: mixed type expression.");
          return null;
        }
        if (lval instanceof Double)
          useDouble = true;

        // Perform the operation base on the type.
        if(!(lval instanceof LinkedList))
        {
            switch(op)
            {
              case ADD:
                if (useDouble)
                  return (Double) lval + (Double) rval;
                return (Integer) lval + (Integer) rval;
              case SUB:
                if (useDouble)
                  return (Double) lval - (Double) rval;
                return (Integer) lval - (Integer) rval;
              case MULT:
                if (useDouble)
                  return (Double) lval * (Double) rval;
                return (Integer)lval * (Integer) rval;
              case DIV:
                if (useDouble)
                  return (Double) lval / (Double) rval;
                return (Integer) lval / (Integer) rval;
              default:
                return null;
            }
        }
        else
            switch(op)
            {
              case CONCAT:
                boolean valid = true;

                // Sets the appropriate type.
                if(((LinkedList) lval).element() instanceof Double)
                {
                    if(((LinkedList) rval).element() instanceof Integer)
                        valid = false;                  
                }
                else
                {
                    if(((LinkedList) rval).element() instanceof Double)
                        valid = false;
                }
                
                if(valid) {
                    if(((LinkedList) lval).addAll((LinkedList) rval))
                    {
                        return lval;
                    }
                }
                else
                    System.out.println("Error: mixed type list not supported.");
                    
              default:
                return null;
            }

     }
 }
