package ast.nodes;

import lexer.Token;
import lexer.TokenType;
import environment.Environment;
import java.util.LinkedList;

/**
 * This node represents a let expression.
 * @author Zach Kissel
 */
 public class LetNode extends SyntaxNode
 {
   private Token var;
   private SyntaxNode varExpr;
   private SyntaxNode expr;

   /**
    * Constructs a new binary operation syntax node.
    * @param var the variable identifier.
    * @param varExpr the expression that give the varaible value.
    * @param expr the expression that uses the variables value.
    */
    public LetNode(Token var, SyntaxNode varExpr, SyntaxNode expr)
    {
      this.var = var;
      this.varExpr = varExpr;
      this.expr = expr;
    }

    /**
     * Evaluate the node.
     * @param env the executional environment we should evaluate the
     * node under.
     * @return the object representing the result of the evaluation.
     */
     public Object evaluate(Environment env)
     {
        Object theVarExpr;
        
        Environment letScope = env.copy();
        
        theVarExpr = varExpr.evaluate(letScope);
        
        // Eval fails if anything is null.
        if(theVarExpr == null)
            return null;
        
        // Make sure the type is sound.
        if(!(theVarExpr instanceof Integer || theVarExpr instanceof Double || 
                theVarExpr instanceof LinkedList))
          return null;
        
        letScope.updateEnvironment(var, theVarExpr);
        
        return expr.evaluate(letScope);
     }
 }
