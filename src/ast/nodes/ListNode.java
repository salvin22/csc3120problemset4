package ast.nodes;

import lexer.Token;
import lexer.TokenType;
import environment.Environment;
import java.util.LinkedList;

/**
 * This node represents the program nonterminal.
 * @author Nicholas Salvi and Gary Seip
 */
 public class ListNode extends SyntaxNode
 {
   private final LinkedList theList;

   /**
    * Constructs a new binary operation syntax node.
     * @param list
    */
    public ListNode(LinkedList list)
    {
      theList = list;
    }
    
    public LinkedList getList()
    {
        return this.theList;
    }
    /**
     * Evaluate the node.
     * @param env the executional environment we should evaluate the
     * node under.
     * @return the object representing the result of the evaluation.
     */
     public Object evaluate(Environment env)
     {
        // It should not be possible to be passed a null l
        if(theList == null)
            return null;
        
        // Returns an empty list if theList is empty.
        if(theList.isEmpty())
        {
            System.out.println("empty list!");
            return null;
        }
        
        // retList is the list to return.
        LinkedList retList = new LinkedList();
        
        // typeChecker serves to ensure that all elements of the list are the same type.
        Object typeChecker;
        // type holds the TokenType of the first element in the list.
        TokenType type;
        // add determines whether to continue adding elements to retList or return null.
        boolean add;
        
        if(theList.element() instanceof Double || theList.element() instanceof Integer)// instance is already a int or double
        {
            if(theList.element() instanceof Double)
                type = TokenType.REAL;
            else
                type = TokenType.INT;


            // COnstructs the evaluated lists.
            for(int i = 0; i < theList.size(); i++)
            {
                typeChecker = theList.get(i);
                retList.add(typeChecker);
            }
        return retList;
        }
        else
        {
       // Gets the evaluation of the first element in the list.
        typeChecker = ((TokenNode) theList.element()).evaluate(env);
        
        // typeChecker should always be Double or Integer in a succesful parse.
        if(!(typeChecker instanceof Double || typeChecker instanceof Integer))
            return null;
        
        // Sets the appropriate type.
        if(typeChecker instanceof Double)
            type = TokenType.REAL;
        else
            type = TokenType.INT;
        
        // COnstructs the evaluated lists.
        for(int i = 0; i < theList.size(); i++)
        {
            typeChecker = ((TokenNode) theList.get(i)).evaluate(env);
            
            if(type == TokenType.REAL)
                add = typeChecker instanceof Double;
            else
                add = typeChecker instanceof Integer;
            
            // If add is false, there is a type error.
            if(add)
                retList.add(typeChecker);
            else
            {
                System.out.println("Mixed mode list not supported");
                return null;
            }
            
            add = false;
        }
        return retList;
        }
     
     }
 }
